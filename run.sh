#!/bin/bash

set -e -x

docker build -t demo .
docker run --rm --name demo-1 -p80:80 demo
