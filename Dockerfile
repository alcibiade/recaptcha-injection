### Build the frontend single page web application.

FROM node:12 as builder

RUN npm install -g @angular/cli

RUN mkdir -p /app
COPY frontend /app/frontend

WORKDIR /app/frontend
RUN npm install
RUN ng build --prod

### Build the HTTP runtime environment.

FROM httpd:2.4-alpine

ENV CAPTCHA_KEY=default_captcha_key

RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /app/frontend/dist/* htdocs/

CMD dockerize -template htdocs/assets/js/captcha-keys.tmpl:htdocs/assets/js/captcha-keys.js httpd-foreground